const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {

  let body;
  let statusCode;

  try {
    switch (event.httpMethod) {
      case 'GET':
        body = await dynamo.scan({ TableName: event.queryStringParameters.TableName }).promise();
        break;
      case 'POST':

        const eventBody = JSON.parse(event.body);
        const params = {
          Key: { "id": eventBody.Item.id },
          TableName: event.queryStringParameters.TableName
        };

        const query = await dynamo.get(params).promise();
        const existingTableItem = query.Item;
        let updated;

        if (existingTableItem) {
          updated = existingTableItem.value === eventBody.Item.value ? existingTableItem.updated : existingTableItem.updated + 1;
        }

        eventBody.Item['updated'] = existingTableItem ? updated : 1;
        body = await dynamo.put(eventBody).promise();
        statusCode = '200';
        break;

      default:
        throw new Error(`Unsupported method "${event.httpMethod}"`);
    }
  } catch (err) {
    statusCode = '400';
    body = err.message;
  } finally {
    body = JSON.stringify(body);
  }

  return {
    statusCode,
    body
  };
};
