# Speech Grapics Test - DynamoDB
This repository contains the Lambda function used to interact with the DynamoDB instance.

## Modules Used
The language used for the function was NodeJS. It uses the AWS SDK for all of the calls to the DynamoDB. The CI/CD pipelines are ran within this repository.

## Function Details
The function will decide what to do based on what HTTP method is specified, if it is a POST, it will take the JSON body and update that record in the DynamoDB if it exists,
if not, it will add it as a new record. The `updated` flag will only be incremented if the value has changed. I.e. if the same POST request is fired two times in a row, it wont register 
that as 2 updates. This functionality at the moment requires the column to be specified as value, however I have detailed that going forward, this would ideally be mandatory.

If the method is a GET it will return all records in the database.

## CI/CD Process
Upon merging into master, it will package up the function, and use the version number in the `version.txt` to store this ZIP file in its corresponding s3 bucket. It will
also update the .txt file containing the current version, also contained in S3, which is used by the terraform when applying. The process will also update the code currently in Lambda. There is 
a manual step to kick off the terraform master pipeline, however this is unnecessary if just testing code changes, as this is handled in the previous step.

## Improvements / Difficulties
An improvement I would make is to have the API gateway handle the model of the request body so that it will always be the same database columns.

## Testing
Testing is detailed in the README in the [Terraform](https://gitlab.com/speech-graphics-assessment/terraform) repository.
